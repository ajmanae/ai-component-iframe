import React,{useState,useEffect} from 'react';
import { withStyles } from '@material-ui/core/styles';
import {IconButton,Typography,Tooltip,CircularProgress,Box,TextField,Button,Grid,Dialog,DialogTitle as MuiDialogTitle,DialogContent as MuiDialogContent,DialogActions as MuiDialogActions} from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import axios from "axios";
import VideocamIcon from '@material-ui/icons/Videocam';

// const baseURL = "http://localhost:40001";
const baseURL = "https://avanza-training.westeurope.cloudapp.azure.com"

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6" style={{color:"#5ED4E4"}}>{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CancelIcon style={{color:"#1762B8"}} />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

var theStream;
let video;
var theRecorder;
var recordedChunks = [];

export default function CustomizedDialogs({open,handleClose}) {


    const [recorded,setRecorded]=useState(false);
    const [videoPermission,setVideoPermission]=useState(true);
    const [videoError,setVideoError]=useState(null);
    const [loaded,setLoaded]=useState(false);
    const [videoFile,setVideoFile]=useState(null);
    const [info,setInfo]=useState({
      nationality:"",
      gender:null,
      emotion:null,
      confidence:0,
      age:"",
    });
    const [faceResponses,setFaceResponses]=useState([]);
  const [timer,setTimer]=useState(60);
  const [fileName,setFileName]=useState(null);
  const [recordingStatus,setRecordingStatus]=useState("start");
  const [submitLoading,setSubmitLoading]=useState(false);
  const [interactionId,setInteractionId] = useState(null);
  const [satisfaction,setSatisfaction] = useState(null);
  const [feedbackLoading,setFeedbackLoading]=useState(false);
  const [completed,setCompleted]=useState(false);
  const [other,setOther]=useState(false);
  let interval;

 
  useEffect(()=>{
    if(open && !loaded){
        video = document.querySelector("#videoElement");
        if (navigator.mediaDevices.getUserMedia) {
          navigator.mediaDevices.getUserMedia({ video: true,audio:true })
            .then((stream)=> {
              theStream = stream;
              video.srcObject = stream;
              setVideoPermission(true);
              try {
                var recorder = new MediaRecorder(stream, {
                  mimeType: "video/webm"
                });
              } catch (e) {
                console.error('Exception while creating MediaRecorder: ' + e);
                return;
              }
            
              theRecorder = recorder;
              recorder.ondataavailable =
                (event) => {
                  recordedChunks.push(event.data);
                };
              // recorder.start(100);
              setLoaded(true);
             
            })
            .catch(function (error) {
              setVideoPermission(false)
              console.log("Something went wrong!",error);
            });
        }
       
    }
    if(recordingStatus=="stop"){
       interval = setInterval(() => {
        setTimer(timer-1);
      }, 1000);
    }
    if(timer==0){
      handleRecording("start");
    }
    if((timer%5==0) && (timer!=60)){
      handleFaceCapture();
    }

    return () => clearInterval(interval);
   

  },[timer,recordingStatus,loaded,videoPermission])


  const handleRecording =(status)=>{
    setVideoError(null);
    if(status=="start"){
      setTimer(60);
      setRecorded(true);
      download();
      //aggregateFaceResponses();
    }
    if(status=="stop"){
      setRecorded(false);
      setFileName(new Date().getTime());
      theRecorder.start(100)
    }
    setRecordingStatus(status);
  }

  const handleAccessToken= async ()=>{
    try {
      let response = await axios.post(`${baseURL}/login`,{
        "userId":"admin_core",
        "password":"2bf3e759dba1b4402707738242f46d16a629f85ab34cf9f964290c9a8c578ef9d6a196d31e694104374d82c302c73bf776f2db831fb7434ca27a3c4ddbb64006"
        },{
            method: 'POST',
          headers:{
            'Content-Type': 'application/json',
          }
      });
      console.log(response,"response------------------->");
      return response.data.loginResponse.data.token;
    }catch(err){
        setSubmitLoading(false);
        setTextError("Error in uploading Feedback");
    }
  }

  const handleSubmit =async ()=>{
    setVideoError(null);
   
    if(!recorded){
        setVideoError("Please record your video first!")
    }
    else{
        setSubmitLoading(true);
        try{
            var bodyFormData = new FormData();
            bodyFormData.append('name', fileName);
            bodyFormData.append('file', videoFile);
            bodyFormData.append('type', "V");
            bodyFormData.append('orgId', "AFZ");
            let response = await axios({
              method: 'post',
              url:`${baseURL}/API/Feedback/uploadInteraction`,
              data:bodyFormData,
              headers:{
                'Content-Type': 'multipart/form-data',
                'token': await handleAccessToken()
              }
            });
            console.log("upload response",response);
            if(response.status==200){
              setSubmitLoading(false);
              setRecorded(false);
              setInteractionId(response.data.id)
            }
          }catch(err){
            setSubmitLoading(false);
            setVideoError("Error in uploading video")
          }
    }
   
  }

  const aggregateFaceResponses=(faceResponses)=>{
    let emotion={
      anger: 0,
      contempt: 0,
      disgust: 0,
      fear: 0,
      happiness: 0,
      neutral: 0,
      sadness: 0,
      surprise: 0,
    };
    let confidence=0;
    let age=0;
    let gender=null;
    let majorEmotion=null;

    faceResponses.forEach(response=>{
      gender = response.gender;
      age += response.age;
      Object.keys(response.emotion).map(o=>{
        emotion[o] += response.emotion[o];
      })
    })

    Object.keys(emotion).map(o=>{
      if(emotion[o]/faceResponses.length>confidence){
        confidence=emotion[o]/faceResponses.length;
        majorEmotion=o;
      }
    })

    age = age/faceResponses.length;

    console.log({...info,age,gender,confidence:confidence*100,emotion:majorEmotion});

    setInfo({...info,age,gender,confidence:confidence*100,emotion:majorEmotion});
}

const handleFaceCapture=()=>{
    console.log("capturinggggggggggggggggggg");
    const canvas = document.createElement("canvas");
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;

    canvas.getContext('2d').drawImage(video,0,0);

    // let dataURL = canvas.toDataURL('image/png');
    canvas.toBlob(async function(blob) {
      var file = new File([blob], `face.png`, {type:"image/png"});
      try{
        var bodyFormData = new FormData();
        bodyFormData.append('file', file);
        let response = await axios({
          method: 'post',
          url:`${baseURL}/API/Feedback/faceAttributes`,
          data:bodyFormData,
          headers:{
            'Content-Type': 'multipart/form-data',
            'token': await handleAccessToken()
          }
        });
        if(response.status==200){
          console.log(response.data,"DFFFFFFFFFFFFFFFFFFFFFFFF");
          setFaceResponses([...faceResponses,...[response.data.faceResponse.faceAttributes]]);
          aggregateFaceResponses([...faceResponses,...[response.data.faceResponse.faceAttributes]]);
          // handleVideoIndex();
          // // setSubmitLoading(false);
          // // setinteractionId(true);
        }
      }catch(err){
        console.log("Error in uploading face photo",err)
      }
    }, 'image/png');
    
  }

  
  const download=()=> {
    theRecorder.stop();
    // theStream.getTracks().forEach(track => {
    //   track.stop();
    // });
  
    var blob = new Blob(recordedChunks, {
      type: 'video/mp4'
    });
    var vfile = new File([blob], `${fileName}.mp4`, {type:"video/mp4"});
    setVideoFile(vfile)
    // console.log(file1,"fileee");
    // sendBlobAsBase64(blob);
   
  }

  const handleSatisfaction = async (value,emotion)=>{
      console.log("Satisfaction called",value,emotion);
    if(value!="No"){
        setFeedbackLoading(true);
    }
    let userEmotion;
    if(emotion=="neutral"){
        userEmotion="neutral"
    }
    if(emotion=="happiness"){
        userEmotion="happy"
    }
    if(emotion=="sadness"){
        userEmotion="sad"
    }
    let type = "V";
    
    if(value=="No"){
        setOther(true);
    }
    if(value=="Yes"){
        try{
            let response = await axios({
              method: 'post',
              url:`${baseURL}/API/Feedback/submitInteraction`,
              data:{
                ...info,
                type,
                interactionId,
                "customerProvidedSentiment":userEmotion
                // indexId:indexResposne.indexId,
              },
              headers:{
                'Content-Type': 'application/json',
                'token': await handleAccessToken()
              }
            });
            console.log("Submit response-------------------->",response);
            if(response.status==200){
              setFeedbackLoading(false);
              setCompleted(true);
            }
          }catch(err){
            alert("Error in Submitting Feedback")
            setFeedbackLoading(false);
          }
    }
    
  }
 
  return (
    <div>
      <Dialog 
        onClose={handleClose} 
        aria-labelledby="customized-dialog-title" 
        fullWidth
        onExited={()=>{
          setRecorded(false);
          setVideoPermission(true);
            setVideoError(null);
            setLoaded(false);
              setVideoFile(null);
            setInfo({
                nationality:"",
                gender:null,
                emotion:null,
                confidence:0,
                age:"",
              });
            setFaceResponses([]);
            setTimer(60);
            setFileName(null);
            setRecordingStatus("start");
            setSubmitLoading(false);
            setInteractionId(null);
            setSatisfaction(null);
            setFeedbackLoading(false);
            setCompleted(false);
            setOther(false);
        }}
        // fullScreen
        // maxWidth="xs"
        open={open}>
        <DialogTitle id="customized-dialog-title2" onClose={handleClose}>
          Suggestions & Feedback
        </DialogTitle>
        <DialogContent dividers>
        {!interactionId?<>
            <Grid item  xs={12}>
       <Grid item  xs={12} style={{position:"relative"}}>
          <video autoPlay controls={false} muted id="videoElement" width="100%" height="auto" style={{backgroundColor:"#e0e0e0",transform:"scaleX(-1)",display:"block"}}/>
          {!submitLoading && !interactionId && videoPermission?<>
          {recordingStatus=="start"?<Tooltip disableFocusListener disableTouchListener title="Start Recording">
          <Box p="4px" style={{cursor:"pointer"}} onClick={()=>handleRecording("stop")} bottom="7%" right="47%" width="35px" border="1" bgcolor="white" borderRadius="50%" display="flex" justifyContent="center" position="absolute">
            <Box bgcolor="red" width="30px" height="26px" borderRadius="50%" />
          </Box>
          </Tooltip>:<Tooltip disableFocusListener disableTouchListener title="Stop Recording">
          <Box p="8px" style={{cursor:"pointer"}} onClick={()=>handleRecording("start")} bottom="7%" right="47%" width="35px" border="1" bgcolor="white" borderRadius="50%" display="flex" justifyContent="center" position="absolute">
            <Box bgcolor="red" width="20px" height="18px"  />
          </Box>
          </Tooltip>}
          </>:null}
          {recordingStatus=="stop"?<Box position="absolute" display="flex" top="3%" right="5%" alignItems="center">
            <Box bgcolor="#ff0000a1" width="12px" height="12px" borderRadius="50%" /> <span style={{color:"white",fontWeight:"bold",fontSize:"18px",paddingLeft:"5px"}}>{timer}s</span>
          </Box>:null}
        </Grid>
        <Box width="100%" bgcolor="#1762B8" color="white" py={0.5} display="flex" justifyContent="center" alignItems="center">
                <VideocamIcon fontSize="small" style={{margin:"0px 5px"}}/> Video
            </Box>
        {recorded?<Grid item sm={12} xs={12}>
         <Typography variant="caption" color="textSecondary" >
            Your video is recorded successfully
          </Typography>
        </Grid>:null}
        {videoError?<Grid item sm={12} xs={12}>
         <Typography variant="caption" style={{color:"#f44336"}} >
            {videoError}
          </Typography>
        </Grid>:null}
        </Grid>
        <Grid item xs={12} style={{display:"flex",justifyContent:"center",alignItems:"center"}}>
            <Box width="50%" pt={2} >
                <Button size="small" onClick={handleSubmit} disabled={submitLoading} variant="contained" fullWidth  style={{backgroundColor:"#1762B8",color:"white"}} >Submit{submitLoading?<CircularProgress style={{marginLeft:10,color:"white"}} size={20}  />:null}</Button>
            </Box>
        </Grid></>:
        <>
        {/* !other || !completed  */}
            {!completed ?!other?<><Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                {info.emotion=="happiness"||info.emotion=="neutral"?"Satisfied with our services?":"Unsatisfied with our services?"}
                </Typography>
            </Grid>
            <Grid item xs={12} style={{textAlign:"center"}}>
              {info.gender=="male"  || info.gender==null?
              <>
                {info.emotion=="happiness"?<img src="man-smile1.png" width="80" height="80" />:null}
               {info.emotion=="neutral"?<img src="man-smile2.png" width="80" height="80" />:null}
               {info.emotion=="sadness"?<img src="man-smile3.png" width="80" height="80" />:null}
               {!["happiness","neutral","sadness"].includes(info.emotion)?<img src="man-smile2.png" width="80" height="80" />:null}
               </>: <>
                {info.emotion=="happiness"?<img src="woman-smile1.png" width="80" height="80" />:null}
               {info.emotion=="neutral"?<img src="woman-smile2.png" width="80" height="80" />:null}
               {info.emotion=="sadness"?<img src="woman-smile3.png" width="80" height="80" />:null}
               {!["happiness","neutral","sadness"].includes(info.emotion)?<img src="woman-smile2.png" width="80" height="80" />:null}
               </>}
            </Grid>
            <Grid item xs={12} style={{display:"flex",justifyContent:"center",alignItems:"center",padding:"20px 0px"}}>
               <Button disabled={feedbackLoading} onClick={()=>handleSatisfaction("Yes",info.emotion)} size="small" variant="contained" style={{backgroundColor:"#1762B8",color:"white",marginRight:"5px"}}>
                   Yes{feedbackLoading?<CircularProgress style={{marginLeft:10,color:"white"}}size={15}  />:null}
               </Button>
               <Button disabled={feedbackLoading} onClick={()=>handleSatisfaction("No")} size="small" variant="contained" style={{backgroundColor:"#5ED4E4",color:"white",marginLeft:"5px"}} >
                   No
               </Button>
            </Grid></>:
             !feedbackLoading?<><Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                    Provide your feedback
                </Typography>
            </Grid>
            <Grid item xs={12} style={{textAlign:"center"}}>
            {info.gender=="male" || info.gender==null?<>
            {["happiness","neutral","sadness"].includes(info.emotion)?<>
            {info.emotion!="happiness" && <img src="man-smile1.png" onClick={()=>handleSatisfaction("Yes","happiness")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>}
            {info.emotion!="neutral" && <img src="man-smile2.png" onClick={()=>handleSatisfaction("Yes","neutral")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>}
            {info.emotion!="sadness"  && <img src="man-smile3.png" onClick={()=>handleSatisfaction("Yes","sadness")} width="80" height="80" style={{marginLeft:"5px",cursor:"pointer"}}/>}
            </>:<>
            {<img src="man-smile1.png" onClick={()=>handleSatisfaction("Yes","happiness")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>}
            {<img src="man-smile3.png" onClick={()=>handleSatisfaction("Yes","sadness")} width="80" height="80" style={{marginLeft:"5px",cursor:"pointer"}}/>}
            </>}
            </>:["happiness","neutral","sadness"].includes(info.emotion)?<>
            {info.emotion!="happiness" && <img src="woman-smile1.png" onClick={()=>handleSatisfaction("Yes","happiness")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>}
            {info.emotion!="neutral" && <img src="woman-smile2.png" onClick={()=>handleSatisfaction("Yes","neutral")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>}
            {info.emotion!="sadness"  && <img src="woman-smile3.png" onClick={()=>handleSatisfaction("Yes","sadness")} width="80" height="80" style={{marginLeft:"5px",cursor:"pointer"}}/>}
            </>:<>
            {<img src="woman-smile1.png" onClick={()=>handleSatisfaction("Yes","happiness")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>}
            {<img src="woman-smile3.png" onClick={()=>handleSatisfaction("Yes","sadness")} width="80" height="80" style={{marginLeft:"5px",cursor:"pointer"}}/>}
            </>}
            </Grid>
            </>:<><Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                    Submitting your feedback
                </Typography>
            </Grid>
            <Grid item xs={12} style={{textAlign:"center"}}>
                <CircularProgress style={{marginLeft:10}} color="primary" size={30}  />
            </Grid>
            </>
            :<Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                    Your feedback is submitted successfully
                </Typography>
            </Grid>}
            
        </>}
        </DialogContent>
        {/* <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Save changes
          </Button>
        </DialogActions> */}
      </Dialog>
    </div>
  );
}