import React,{useState} from 'react';
import { withStyles } from '@material-ui/core/styles';
import {CircularProgress,Box,TextField,Button,Grid,Dialog,DialogTitle as MuiDialogTitle,DialogContent as MuiDialogContent,DialogActions as MuiDialogActions} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import CancelIcon from '@material-ui/icons/Cancel';
import Typography from '@material-ui/core/Typography';
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import axios from "axios";

const clientKey = '3d44a382820ca884b74110fc1eebe5984f524343f78e58bd5958ea56fbf195907203c6b4b99fc40d6c5d18ec12bf41f9bde3aa84a47bf6a7b1bbb107c07937a8';
const clientSecret = '70747a4ad98badf11465ae80f45e047a1832f1805ebe91b473295d92500a922dd9b6b41c91ebd34beb0f692b40a303a27a6910f5075c6b6897da639e85335463';
// const baseURL = "http://localhost:30001"
const baseURL = "https://customer-happiness.uaenorth.cloudapp.azure.com/api"

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6" style={{color:"#5ED4E4"}}>{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CancelIcon style={{color:"#1762B8"}} />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function CustomizedDialogs({open,handleClose}) {

  const [textError,setTextError]=useState(null);
  const [textValue,setTextValue]=useState("");
  const [submitLoading,setSubmitLoading]=useState(false);
  const [interaction,setInteraction] = useState(null);
  const [satisfaction,setSatisfaction] = useState(null);
  const [feedbackLoading,setFeedbackLoading]=useState(false);
  const [completed,setCompleted]=useState(false);
  const [other,setOther]=useState(false);
  const [accessToken,setAccessToken]=useState("");

  // const handleAccessToken= async ()=>{
  //   try {
  //     let response = await axios.post(`${baseURL}/login`,{
  //       "userId":"admin_core",
  //       "password":"2bf3e759dba1b4402707738242f46d16a629f85ab34cf9f964290c9a8c578ef9d6a196d31e694104374d82c302c73bf776f2db831fb7434ca27a3c4ddbb64006"
  //       },{
  //           method: 'POST',
  //         headers:{
  //           'Content-Type': 'application/json',
  //         }
  //     });
  //     console.log(response,"response------------------->");
  //     setAccessToken(response.data.loginResponse.data.token);
  //     return response.data.loginResponse.data.token;
  //   }catch(err){
  //       setSubmitLoading(false);
  //       setTextError("Error in uploading Feedback");
  //   }
  // }

  const handleSubmit =async ()=>{
    setTextError(null);
    if(!textValue){
      setTextError("Please enter your feedback")
    }
    else{
      setSubmitLoading(true);
      // let token = await handleAccessToken()
      try{
        let response = await axios({
          method: 'post',
          url:`${baseURL}/PUBLIC/Feedback/quickFeedback`,
          data:{
            type:"T",
            orgId:"AFZ",
            language:"en-US",
            textData:{
              "documents": [
                {
                    "id": "1",
                    "text": textValue
                }
              ]
            }
          },
          headers:{
            'Content-Type': 'application/json',
            'clientKey':clientKey,
            'clientSecret':clientSecret
          }
        });
        console.log("text upload response",response);
        if(response.data){
          setSubmitLoading(false);
          setTextError(null);
          setInteraction(response.data);
          await axios({
            method: 'post',
            url:`${baseURL}/PUBLIC/Feedback/uploadInteraction`,
            data:{
              type:"T",
              orgId:"AFZ",
              interactionId:response.data.interactionId,
              language:"en-US",
              textData:{
                "documents": [
                  {
                      "id": "1",
                      "text": textValue
                  }
                ]
              }
            },
            headers:{
              'Content-Type': 'application/json',
              'clientKey':clientKey,
              'clientSecret':clientSecret
            }
          });
        }
      }catch(err){
        setSubmitLoading(false);
        setTextError("Error in uploading Feedback")
      }
    }
   
  }

  const handleSatisfaction = async (value,userEmotion)=>{
      console.log("Satisfaction called",value,userEmotion);
    if(value!="No"){
        setFeedbackLoading(true);
    }
    let type = "T";
    if(value=="No"){
        setOther(true);
    }
    if(value=="Yes"){
        try{
            let response = await axios({
              method: 'post',
              url:`${baseURL}/PUBLIC/Feedback/submitInteraction`,
              data:{
              //   ...info,
                type,
                interactionId:interaction.interactionId,
                "customerProvidedSentiment":userEmotion
                // indexId:indexResposne.indexId,
              },
              headers:{
                'Content-Type': 'application/json',
                'clientKey':clientKey,
                'clientSecret':clientSecret
              }
            });
            console.log("Submit response-------------------->",response);
            if(response.status==200){
              setFeedbackLoading(false);
              setCompleted(true);
            }
          }catch(err){
            alert("Error in Submitting Feedback")
            setFeedbackLoading(false);
          }
    }
    
  }
 
  return (
    <div>
      <Dialog 
        onClose={handleClose} 
        aria-labelledby="customized-dialog-title" 
        fullWidth
        onExited={()=>{
          setTextError(null);
           setTextValue("");
            setSubmitLoading(false);
            setInteraction(null);
            setSatisfaction(null);
            setFeedbackLoading(false);
            setCompleted(false);
            setOther(false);
        }}
        // fullScreen
        // maxWidth="xs"
        open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Suggestions & Feedback
        </DialogTitle>
        <DialogContent dividers>
        {!interaction?<>
        <Grid item xs={12}>
            <TextField
              id="outlined-multiline-static"
              // label="Type here"
              InputProps={{
                  readOnly:submitLoading
              }}
              placeholder="Enter your feedback..."
              fullWidth
              multiline
              rows={8}
              error={textError?true:false}
              value={textValue}
            //   helperText={textError?textError:`${textValue.length}/250`}
              inputProps={{ maxLength: 250 }}
              onChange={(e)=>{
                setTextError(null);
                setTextValue(e.target.value)
              }}
              // defaultValue="Default Value"
              variant="outlined"
            />
            <Box width="100%" bgcolor="#1762B8" color="white" py={0.5} display="flex" justifyContent="center" alignItems="center">
                <ChatBubbleIcon fontSize="small" style={{margin:"0px 5px"}}/> Text
            </Box>
        </Grid>
        {textError?<Grid item xs={12}>
            <Typography variant="caption" color="error">
                {textError}
            </Typography>
        </Grid>:null}
        <Grid item xs={12} style={{display:"flex",justifyContent:"center",alignItems:"center"}}>
            <Box width="50%" pt={2} >
                <Button size="small" onClick={handleSubmit} disabled={submitLoading} variant="contained" fullWidth  style={{backgroundColor:"#1762B8",color:"white"}} >Submit{submitLoading?<CircularProgress style={{marginLeft:10,color:"white"}} size={20}  />:null}</Button>
            </Box>
        </Grid></>:
        <>
        {/* !other || !completed  */}
            {!completed ?!other?<><Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                {interaction.detectedMajorSentimentText=="positive" || interaction.detectedMajorSentimentText=="neutral" ?"Satisfied with our services?":"Unsatisfied with our services?"}
                </Typography>
            </Grid>
            <Grid item xs={12} style={{textAlign:"center"}}>
            {interaction.detectedMajorSentimentText=="positive"?
             <Box width={1} style={{textAlign:"center"}}>
             <Box>
           <img src="man-smile1.png" width="80" height="80" />
           </Box>
           {/* <Typography variant="caption" color="textSecondary" gutterBottom>
             Satisfied
           </Typography> */}
         </Box>
              :null}
               {/* {interaction.detectedMajorSentimentText=="neutral"?<Box width={1} style={{textAlign:"center"}}>
                   <Box>
                 <img src="man-smile2.png" width="80" height="80" />
                 </Box>
               </Box>:null} */}
               {interaction.detectedMajorSentimentText=="negative"?<Box width={1} style={{textAlign:"center"}}>
                   <Box>
                 <img src="man-smile3.png" width="80" height="80" />
                 </Box>
                 {/* <Typography variant="caption" color="textSecondary" gutterBottom>
                   Unsatisfied
                 </Typography> */}
               </Box>:null}
               {!["positive","neutral","negative"].includes(interaction.detectedMajorSentimentText)?
                 <Box width={1} style={{textAlign:"center"}}>
                   <Box>
                 <img src="man-smile2.png" width="80" height="80" />
                 </Box>
                 {/* <Typography variant="caption" color="textSecondary" gutterBottom>
                   I'm OK
                 </Typography> */}
               </Box>
               :null}
             
            </Grid>
            <Grid item xs={12} style={{display:"flex",justifyContent:"center",alignItems:"center",padding:"20px 0px"}}>
               <Button disabled={feedbackLoading} onClick={()=>handleSatisfaction("Yes",interaction.detectedMajorSentimentText?interaction.detectedMajorSentimentText:"neutral")} size="small" variant="contained" style={{backgroundColor:"#1762B8",color:"white",marginRight:"5px"}}>
                   Yes{feedbackLoading?<CircularProgress style={{marginLeft:10,color:"white"}}size={15}  />:null}
               </Button>
               <Button disabled={feedbackLoading} onClick={()=>handleSatisfaction("No")} size="small" variant="contained" style={{backgroundColor:"#5ED4E4",color:"white",marginLeft:"5px"}} >
                   No
               </Button>
            </Grid></>:
             !feedbackLoading?<><Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                    Provide your feedback
                </Typography>
            </Grid>
            <Grid item xs={12} style={{textAlign:"center",display:"flex",justifyContent:"center"}}>
            {/* {interaction.detectedMajorSentimentText!="positive" &&
            <Box style={{textAlign:"center"}}>
            <Box>
            <img src="man-smile1.png" onClick={()=>handleSatisfaction("Yes","positive")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
          </Box>
          <Typography variant="caption" color="textSecondary" gutterBottom>
            Satisfied
          </Typography>
        </Box> 
            } */}
            {(interaction.detectedMajorSentimentText!="neutral" || interaction.detectedMajorSentimentText!="positive" ) && 
             <Box  style={{textAlign:"center"}}>
             <Box>
             <img src="man-smile2.png" onClick={()=>handleSatisfaction("Yes","neutral")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
           </Box>
           <Typography variant="caption" color="textSecondary" gutterBottom>
             I'm OK
           </Typography>
         </Box> }
            {interaction.detectedMajorSentimentText!="negative"  && 
             <Box style={{textAlign:"center"}}>
             <Box>
             <img src="man-smile3.png" onClick={()=>handleSatisfaction("Yes","negative")} width="80" height="80" style={{marginLeft:"5px",cursor:"pointer"}}/>
           </Box>
           <Typography variant="caption" color="textSecondary" gutterBottom>
             Unsatisfied
           </Typography>
         </Box>
           
            }
            </Grid>
            </>:<><Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                    Submitting your feedback
                </Typography>
            </Grid>
            <Grid item xs={12} style={{textAlign:"center"}}>
                <CircularProgress style={{marginLeft:10}} color="primary" size={30}  />
            </Grid>
            </>
            :<Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                    Your feedback is submitted successfully
                </Typography>
            </Grid>}
            
        </>}
        </DialogContent>
        {/* <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Save changes
          </Button>
        </DialogActions> */}
      </Dialog>
    </div>
  );
}