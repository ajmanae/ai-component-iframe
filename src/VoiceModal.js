import React,{useState,useEffect} from 'react';
import { withStyles } from '@material-ui/core/styles';
import {IconButton,Typography,Tooltip,FormControlLabel,Checkbox,CircularProgress,Box,Menu,MenuItem,TextField,Button,Grid,Dialog,DialogTitle as MuiDialogTitle,DialogContent as MuiDialogContent,DialogActions as MuiDialogActions} from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import axios from "axios";
import MicRecorder from 'mic-recorder-to-mp3';
import MicIcon from '@material-ui/icons/Mic';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

const clientKey = '3d44a382820ca884b74110fc1eebe5984f524343f78e58bd5958ea56fbf195907203c6b4b99fc40d6c5d18ec12bf41f9bde3aa84a47bf6a7b1bbb107c07937a8';
const clientSecret = '70747a4ad98badf11465ae80f45e047a1832f1805ebe91b473295d92500a922dd9b6b41c91ebd34beb0f692b40a303a27a6910f5075c6b6897da639e85335463';
// const baseURL = "http://localhost:30001";
const baseURL = "https://customer-happiness.uaenorth.cloudapp.azure.com/api"

const recorder = new MicRecorder({
    bitRate: 128
  });

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h5" style={{color:"#5ED4E4"}}>{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CancelIcon style={{color:"#1762B8"}} />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function CustomizedDialogs({open,handleClose}) {

  const [audioError,setAudioError]=useState(null);
  const [audioFile,setAudioFile]=useState(null);
  const [arecorded,setARecorded]=useState(false);
  const [timer,setTimer]=useState(30);
  const [fileName,setFileName]=useState(null);
  const [recordingStatus,setRecordingStatus]=useState("start");
  const [submitLoading,setSubmitLoading]=useState(false);
  const [interaction,setInteraction] = useState(null);
  const [satisfaction,setSatisfaction] = useState(null);
  const [feedbackLoading,setFeedbackLoading]=useState(false);
  const [completed,setCompleted]=useState(false);
  const [other,setOther]=useState(false);
  const [accessToken,setAccessToken]=useState("");
  const [anchorEl, setAnchorEl] = useState(null);
  const [language,setLanguage] = useState("en-US");
  const [lname,setlname]=useState("English");
  const [disc,setDisc] = useState(false);
  let interval;

  useEffect(()=>{
    if(recordingStatus=="stop"){
       interval = setInterval(() => {
        setTimer(timer-1);
      }, 1000);
    }
    if(timer==0){
      handleRecording("start");
    }

    return () => clearInterval(interval);
   

  },[timer,recordingStatus])


  const handleAudio =(status)=>{
    setAudioError(null)
    if(status=="start"){
      recorder.stop().getMp3().then(([buffer, blob]) => {
        if(document.querySelector('#playlist').hasChildNodes()){
          document.querySelector('#playlist').removeChild(document.querySelector('#playlist').firstChild)
        }
        const file = new File(buffer, `${fileName}.mp3`, {
          type: blob.type,
          lastModified: Date.now()
        });
        setAudioFile(file);
        const li = document.createElement('li');
        const player = new Audio(URL.createObjectURL(file));
        player.controls = true;
        li.appendChild(player);
        document.querySelector('#playlist').appendChild(li);
        setTimer(30);
        setARecorded(true);
        setRecordingStatus(status);
      }).catch((e) => {
        console.error(e);
      });
    
    }
    if(status=="stop"){
      if(document.querySelector('#playlist').hasChildNodes()){
        document.querySelector('#playlist').removeChild(document.querySelector('#playlist').firstChild)
      }
      recorder.start().then(() => {
        setRecordingStatus(status);
        setARecorded(false);
        setFileName(new Date().getTime());
      }).catch((e) => {
        console.error(e);
      });
     
    }
    
  }

  // const handleAccessToken= async ()=>{
  //   try {
  //     let response = await axios.post(`${baseURL}/login`,{
  //       "userId":"admin_core",
  //       "password":"2bf3e759dba1b4402707738242f46d16a629f85ab34cf9f964290c9a8c578ef9d6a196d31e694104374d82c302c73bf776f2db831fb7434ca27a3c4ddbb64006"
  //       },{
  //           method: 'POST',
  //         headers:{
  //           'Content-Type': 'application/json',
  //         }
  //     });
  //     console.log(response,"response------------------->");
  //     setAccessToken(response.data.loginResponse.data.token);
  //     return response.data.loginResponse.data.token;
  //   }catch(err){
  //       setSubmitLoading(false);
  //       setAudioError("Error in uploading Feedback");
  //   }
  // }

  const handleSubmit =async ()=>{
    setAudioError(null);
    // let cinteractionId=`A-${new Date().getTime()}`;
    if(!arecorded){
        setAudioError("Please record your audio!")
    }
    else{
        setSubmitLoading(true);
        // let token = await handleAccessToken();
        try{
          var bodyFormData = new FormData();
          bodyFormData.append('type', "A");
          bodyFormData.append('orgId', "AFZ");
          bodyFormData.append('language', language);
          bodyFormData.append('file', audioFile);
          bodyFormData.append('name', fileName);
          let response = await axios({
            method: 'post',
            url:`${baseURL}/PUBLIC/Feedback/quickFeedback`,
            data:bodyFormData,
            headers:{
              'Content-Type': 'multipart/form-data',
              'clientKey':clientKey,
              'clientSecret':clientSecret
            }
          });
          console.log("upload response",response);
          if(response.status==200){
          
            // setInteraction(response.data.interactionId);
            try{
              var bodyFormData1 = new FormData();
              bodyFormData1.append('name', fileName);
              bodyFormData1.append('file', audioFile);
              bodyFormData1.append('language', language);
              bodyFormData1.append('type', "A");
              bodyFormData1.append('orgId', "AFZ");
              bodyFormData1.append('interactionId',response.data.interactionId)
              let response1 = await axios({
                method: 'post',
                url:`${baseURL}/PUBLIC/Feedback/uploadInteraction`,
                data:bodyFormData1,
                headers:{
                  'Content-Type': 'multipart/form-data',
                  'clientKey':clientKey,
                  'clientSecret':clientSecret
                }
              });
              console.log("upload response",response1);
              setSubmitLoading(false);
              setInteraction(response.data);
              // if(response.status==200){
              //   // setSubmitLoading(false);
              //   // setInteraction(response.data.id)
              //   // handleAudioIndex();
              //   // setSubmitLoading(false);
              //   // setEmoji(true);
              // }
            }catch(err){
              setSubmitLoading(false);
              setAudioError("Error in uploading audio")
            }
          }
        }catch(err){
          setSubmitLoading(false);
          setAudioError("Error in uploading audio");
        }
        
    }
   
  }

  const handleSatisfaction = async (value,userEmotion)=>{
      console.log("Satisfaction called",value,userEmotion);
    if(value!="No"){
        setFeedbackLoading(true);
    }
    let type = "A";
    if(value=="No"){
        setOther(true);
    }
    if(value=="Yes"){
        try{
            let response = await axios({
              method: 'post',
              url:`${baseURL}/PUBLIC/Feedback/submitInteraction`,
              data:{
              //   ...info,
                type,
                interactionId:interaction.interactionId,
                "customerProvidedSentiment":userEmotion
                // indexId:indexResposne.indexId,
              },
              headers:{
                'Content-Type': 'application/json',
                'clientKey':clientKey,
                'clientSecret':clientSecret
              }
            });
            console.log("Submit response-------------------->",response);
            if(response.status==200){
              setFeedbackLoading(false);
              setCompleted(true);
            }
          }catch(err){
            alert("Error in Submitting Feedback")
            setFeedbackLoading(false);
          }
    }
    
  }

  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };
  
  const handleLanguage= (language,name) => {
    setLanguage(language);
    setlname(name);
    // if(language!="English"){
    //   document.getElementsByTagName('html')[0].setAttribute("dir", "rtl");
    // }
    // else{
    //   document.getElementsByTagName('html')[0].setAttribute("dir", "ltr");
    // }
    setAnchorEl(null);
  };
  
 
  return (
    <div>
      <Dialog 
        onClose={handleClose} 
        aria-labelledby="customized-dialog-title1" 
        fullWidth
        onExited={()=>{
            setAudioError(null);
            setAudioFile(null);
            setARecorded(false);
            setTimer(60);
            setFileName(null);
            setRecordingStatus("start");
            setSubmitLoading(false);
            setInteraction(null);
            setSatisfaction(null);
            setFeedbackLoading(false);
            setCompleted(false);
            setOther(false);
        }}
        // fullScreen
        // maxWidth="xs"
        open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Suggestions & Feedback
        </DialogTitle>
        <DialogContent dividers>
        {!interaction?<>
            <Grid  item container xs={12} >
            <Box border={1} borderColor="#d5d5d5" width={1} borderRadius="4px" position="relative">
                {!submitLoading?<Box>
                  <Grid item  xs={12} style={{display:"flex",justifyContent:"center",margin:"5px 0px 0px 0px",textAlign:"center"}}>
                  <Typography variant="body2" color="textSecondary" >
                    Press the below button to record your feedback in
                  </Typography>
                </Grid>
                <Grid item  xs={12} style={{display:"flex",justifyContent:"center",margin:"0px 0px 0px 0px",textAlign:"center"}}>
                <Button variant="text" color="primary" size="small" aria-controls="simple-menu" aria-haspopup="true" onClick={handleMenuOpen}>
                  {lname} <ArrowDropDownIcon/>
                </Button>
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={()=>setAnchorEl(null)}
                    
                  >
                    <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("en-US","English")}>English</MenuItem>
                    <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("ar-EG","Arabic")}>Arabic</MenuItem>
                    <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("hi-IN","Urdu")}>Urdu</MenuItem>
                    <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("hi-IN","Hindi")}>Hindi</MenuItem>
                    <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("auto","Other")}>Other</MenuItem>
                  </Menu>
                </Grid>
                {recordingStatus=="start"?<Grid item  xs={12} style={{display:"flex",justifyContent:"center",margin:"20px 0px"}}>
                <Tooltip disableFocusListener disableTouchListener title="Start Recording">
                    <img src="mic-on.png" style={{cursor:"pointer",height:"125px",width:"125px"}} onClick={()=>handleAudio("stop")}/>
                    </Tooltip>
                </Grid>:
                <Grid item  xs={12} style={{display:"flex",justifyContent:"center",margin:"20px 0px"}}>
                <Tooltip disableFocusListener disableTouchListener title="Stop Recording">
                    <img src="stop.png" style={{cursor:"pointer",height:"125px",width:"125px"}} onClick={()=>handleAudio("start")}/>
                    </Tooltip>
                
                </Grid>}
                </Box>:null}
                {recordingStatus=="stop"?<Box position="absolute" display="flex" top="12%" right="5%" alignItems="center">
                <Box bgcolor="#ff0000a1" width="12px" height="12px" borderRadius="50%" /> <span style={{color:"rgba(108, 117, 125, 0.5)",fontWeight:"bold",fontSize:"18px",paddingLeft:"5px"}}>{timer}s</span>
                </Box>:null}
                <Grid item  xs={12} style={{display:arecorded?"flex":"none",justifyContent:"center"}}>
                <ul id="playlist" style={{padding:0,listStyleType:"none"}}></ul>
                </Grid>
                <Box width="100%" bgcolor="#1762B8" color="white" py={0.5} display="flex" justifyContent="center" alignItems="center">
                <MicIcon fontSize="small" style={{margin:"0px 5px"}}/> Audio
                </Box>
            </Box>
        {audioError?<Grid item  xs={12}>
         <Typography variant="caption" style={{color:"#f44336"}} >
            {audioError}
          </Typography>
        </Grid>:null}
        </Grid>
        <Grid item xs={12} style={{display:"flex",alignItems:"center",paddingTop:"10px"}}>
        <Checkbox color="primary" checked={disc} onChange={(e)=>setDisc(e.target.checked)} name="disc" />
        <Typography variant="caption" color="textSecondary">
            I consent usage of this recorded data for the purpose of quality assurance
            </Typography>
          
        </Grid>
        <Grid item xs={12} style={{display:"flex",justifyContent:"center",alignItems:"center"}}>
            <Box width="50%" pt={2} >
                <Button size="small" onClick={handleSubmit} disabled={!disc || submitLoading} variant="contained" color="primary" fullWidth  style={{}} >Submit{submitLoading?<CircularProgress style={{marginLeft:10,color:"white"}} size={20}  />:null}</Button>
            </Box>
        </Grid></>:
        <>
        {/* !other || !completed  */}
            {!completed ?!other?<><Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                {interaction.detectedMajorSentimentAudio=="neutral" || interaction.detectedMajorSentimentAudio=="positive" ?"Satisfied with our services?":"Unsatisfied with our services?"}
                </Typography>
            </Grid>
            <Grid item xs={12} style={{textAlign:"center"}}>
            {interaction.detectedMajorSentimentAudio=="positive"?
             <Box width={1} style={{textAlign:"center"}}>
             <Box>
           <img src="man-smile1.png" width="80" height="80" />
           </Box>
           {/* <Typography variant="caption" color="textSecondary" gutterBottom>
             Satisfied
           </Typography> */}
         </Box>
              :null}
               {/* {interaction.detectedMajorSentimentAudio=="neutral"?<Box width={1} style={{textAlign:"center"}}>
                   <Box>
                 <img src="man-smile2.png" width="80" height="80" />
                 </Box>
               </Box>:null} */}
               {interaction.detectedMajorSentimentAudio=="negative"?<Box width={1} style={{textAlign:"center"}}>
                   <Box>
                 <img src="man-smile3.png" width="80" height="80" />
                 </Box>
                 {/* <Typography variant="caption" color="textSecondary" gutterBottom>
                   Unsatisfied
                 </Typography> */}
               </Box>:null}
               {!["positive","neutral","negative"].includes(interaction.detectedMajorSentimentAudio)?
                 <Box width={1} style={{textAlign:"center"}}>
                   <Box>
                 <img src="man-smile2.png" width="80" height="80" />
                 </Box>
                 {/* <Typography variant="caption" color="textSecondary" gutterBottom>
                   I'm OK
                 </Typography> */}
               </Box>
               :null}
            </Grid>
            <Grid item xs={12} style={{display:"flex",justifyContent:"center",alignItems:"center",padding:"20px 0px"}}>
               <Button disabled={feedbackLoading} onClick={()=>handleSatisfaction("Yes",interaction.detectedMajorSentimentAudio?interaction.detectedMajorSentimentAudio:"neutral")} size="small" variant="contained" style={{backgroundColor:"#1762B8",color:"white",marginRight:"5px"}}>
                   Yes{feedbackLoading?<CircularProgress style={{marginLeft:10,color:"white"}}size={15}  />:null}
               </Button>
               <Button disabled={feedbackLoading} onClick={()=>handleSatisfaction("No")} size="small" variant="contained" style={{backgroundColor:"#5ED4E4",color:"white",marginLeft:"5px"}} >
                   No
               </Button>
            </Grid></>:
             !feedbackLoading?<><Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                    Provide your feedback
                </Typography>
            </Grid>
            <Grid item xs={12} style={{textAlign:"center",display:"flex",justifyContent:"center"}}>
            {/* {interaction.detectedMajorSentimentAudio!="positive" &&
            <Box style={{textAlign:"center"}}>
            <Box>
            <img src="man-smile1.png" onClick={()=>handleSatisfaction("Yes","positive")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
          </Box>
          <Typography variant="caption" color="textSecondary" gutterBottom>
            Satisfied
          </Typography>
        </Box> 
            } */}
            {(interaction.detectedMajorSentimentAudio!="neutral" || interaction.detectedMajorSentimentAudio!="positive") && 
             <Box  style={{textAlign:"center"}}>
             <Box>
             <img src="man-smile2.png" onClick={()=>handleSatisfaction("Yes","neutral")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
           </Box>
           <Typography variant="caption" color="textSecondary" gutterBottom>
             I'm OK
           </Typography>
         </Box> }
            {interaction.detectedMajorSentimentAudio!="negative"  && 
             <Box style={{textAlign:"center"}}>
             <Box>
             <img src="man-smile3.png" onClick={()=>handleSatisfaction("Yes","negative")} width="80" height="80" style={{marginLeft:"5px",cursor:"pointer"}}/>
           </Box>
           <Typography variant="caption" color="textSecondary" gutterBottom>
             Unsatisfied
           </Typography>
         </Box>
           
            }
            </Grid>
            </>:<><Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                    Submitting your feedback
                </Typography>
            </Grid>
            <Grid item xs={12} style={{textAlign:"center"}}>
                <CircularProgress style={{marginLeft:10}} color="primary" size={30}  />
            </Grid>
            </>
            :<Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                    Your feedback is submitted successfully
                </Typography>
            </Grid>}
            
        </>}
        </DialogContent>
        {/* <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Save changes
          </Button>
        </DialogActions> */}
      </Dialog>
    </div>
  );
}