import React,{useState,useEffect} from 'react';
import {Container,Typography,Box,Grid,TextField,Button,ButtonGroup,CircularProgress,Grow,Tooltip,Divider,Menu,MenuItem,Select,FormControl,InputLabel} from '@material-ui/core';
import Link from '../src/Link';
import MicIcon from '@material-ui/icons/Mic';
import MicNoneIcon from '@material-ui/icons/MicNone';
import MicOffIcon from '@material-ui/icons/MicOff';
import VideocamIcon from '@material-ui/icons/Videocam';
import VideocamOffIcon from '@material-ui/icons/VideocamOff';
import TelegramIcon from '@material-ui/icons/Telegram';
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import MicRecorder from 'mic-recorder-to-mp3';
import axios from 'axios';
import _ from "lodash";
const recorder = new MicRecorder({
  bitRate: 128
});


const baseURL = "https://avanza-training.westeurope.cloudapp.azure.com"
// const baseURL = "http://localhost:40001"

export default function Index() {

  const [selected,setSelected]=useState("text");
  const [textError,setTextError]=useState(null);
  const [textValue,setTextValue]=useState("");
  const [submitLoading,setSubmitLoading]=useState(false);
  const [emoji,setEmoji]=useState(false);
  const [recordingStatus,setRecordingStatus]=useState("start");
  const [recorded,setRecorded]=useState(false);
  const [arecorded,setARecorded]=useState(false);
  const [videoPermission,setVideoPermission]=useState(true);
  const [videoError,setVideoError]=useState(null);
  // const [audioStatus,setAudioStatus]=useState("start");
  const [audioError,setAudioError]=useState(null);
  const [completed,setCompleted]=useState(false);
  const [timer,setTimer]=useState(60);
  // const [timer,setAtimer]=useState(60);
  const [loaded,setLoaded]=useState(false);
  const [accessToken,setAccessToken]=useState(null);
  const [fileName,setFileName]=useState(null);
  const [videoFile,setVideoFile]=useState(null);
  const [audioFile,setAudioFile]=useState(null);
  const [indexResposne,setIndexResponse] = useState(null);
  const [feedbackLoading,setFeedbackLoading]=useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [interactionId,setInteractionId] = useState(null);
  const [info,setInfo]=useState({
    nationality:"",
    gender:null,
    emotion:null,
    confidence:0,
    age:"",
    // gender:""
  });
  // const [faceAPICount,setAPICount]=useState(0);
  const [faceResponses,setFaceResponses]=useState([]);
  // const [aggregatedResponse,setAggregatedResponse]=useState({
  //   age:0,
  //   gender:null,
  //   emotion:null,
  //   confidence:0
  // })

  let interval;
  var theStream;
let video;
var theRecorder;
var recordedChunks = [];

  useEffect(()=>{
    if(selected=="video" && !loaded){
        video = document.querySelector("#videoElement");
        if (navigator.mediaDevices.getUserMedia) {
          navigator.mediaDevices.getUserMedia({ video: true,audio:true })
            .then((stream)=> {
              theStream = stream;
              video.srcObject = stream;
              setVideoPermission(true);
              try {
                var recorder = new MediaRecorder(stream, {
                  mimeType: "video/webm"
                });
              } catch (e) {
                console.error('Exception while creating MediaRecorder: ' + e);
                return;
              }
            
              theRecorder = recorder;
              recorder.ondataavailable =
                (event) => {
                  recordedChunks.push(event.data);
                };
              // recorder.start(100);
              setLoaded(true);
             
            })
            .catch(function (error) {
              setVideoPermission(false)
              console.log("Something went wrong!",error);
            });
        }
       
    }
    if(recordingStatus=="stop"){
       interval = setInterval(() => {
        setTimer(timer-1);
      }, 1000);
    }
    if(timer==0){
      handleRecording("start");
    }
    if(selected=="video" && (timer%5==0) && (timer!=60)){
      handleFaceCapture();
    }

    return () => clearInterval(interval);
   

  },[selected,timer,recordingStatus])

  const handleOption =(option)=>{
    setSelected(option);
  }

  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleLanguage= (language) => {
    if(language!="English"){
      document.getElementsByTagName('html')[0].setAttribute("dir", "rtl");
    }
    else{
      document.getElementsByTagName('html')[0].setAttribute("dir", "ltr");
    }
    setAnchorEl(null);
  };

  const handleSubmit =async ()=>{
    setTextError(null);
    setVideoError(null);
    setAudioError(null);
   
    if(selected=="text" && !textValue){
      setTextError("Please enter your feedback")
    }
    else if(selected=="video" && !recorded){
      setVideoError("Please record your video first")
    }
    else if(selected=="audio" && !arecorded){
      setAudioError("Please record your audio first")
    }
    else if(selected=="video" && videoFile){
      setSubmitLoading(true);
      try{
        var bodyFormData = new FormData();
        bodyFormData.append('name', fileName);
        bodyFormData.append('file', videoFile);
        bodyFormData.append('type', "V");
        bodyFormData.append('orgId', "AFZ");
        let response = await axios({
          method: 'post',
          url:`${baseURL}/API/Feedback/uploadInteraction`,
          data:bodyFormData,
          headers:{
            'Content-Type': 'multipart/form-data',
            'token': await handleAccessToken()
          }
        });
        console.log("upload response",response);
        if(response.status==200){
          setSubmitLoading(false);
          setRecorded(false);
          setEmoji(true);
          setInteractionId(response.data.id)
          // handleVideoIndex();
          // setSubmitLoading(false);
          // setEmoji(true);
        }
      }catch(err){
        setSubmitLoading(false);
        alert("Error in uploading video")
      }
    }
    else if(selected=="audio" && audioFile){
      setSubmitLoading(true);
      try{
        var bodyFormData = new FormData();
        bodyFormData.append('name', fileName);
        bodyFormData.append('file', audioFile);
        bodyFormData.append('type', "A");
        bodyFormData.append('orgId', "AFZ");
        let response = await axios({
          method: 'post',
          url:`${baseURL}/API/Feedback/uploadInteraction`,
          data:bodyFormData,
          headers:{
            'Content-Type': 'multipart/form-data',
            'token': await handleAccessToken()
          }
        });
        console.log("upload response",response);
        if(response.status==200){
          setSubmitLoading(false);
          setEmoji(true);
          setInteractionId(response.data.id)
          // handleAudioIndex();
          // setSubmitLoading(false);
          // setEmoji(true);
        }
      }catch(err){
        setSubmitLoading(false);
        alert("Error in uploading audio")
      }
    }
    else{
      setSubmitLoading(true);
      try{
        let response = await axios({
          method: 'post',
          url:`${baseURL}/API/Feedback/uploadInteraction`,
          data:{
            type:"T",
            orgId:"AFZ",
            textData:{
              "documents": [
                {
                    "id": "1",
                    "text": textValue
                }
              ]
            }
          },
          headers:{
            'Content-Type': 'application/json',
            'token': await handleAccessToken()
          }
        });
        console.log("text upload response",response);
        if(response.data){
          setSubmitLoading(false);
          setEmoji(true);
          setInteractionId(response.data.id)
        }
      }catch(err){
        setSubmitLoading(false);
        alert("Error in uploading video")
      }
    }
   
  }

  const handleRecording =(status)=>{
    setVideoError(null);
    if(status=="start"){
      setTimer(60);
      setRecorded(true);
      download();
      //aggregateFaceResponses();
    }
    if(status=="stop"){
      setRecorded(false);
      setFileName(new Date().getTime());
      theRecorder.start(100)
    }
    setRecordingStatus(status);
  }

  const aggregateFaceResponses=(faceResponses)=>{
      let emotion={
        anger: 0,
        contempt: 0,
        disgust: 0,
        fear: 0,
        happiness: 0,
        neutral: 0,
        sadness: 0,
        surprise: 0,
      };
      let confidence=0;
      let age=0;
      let gender=null;
      let majorEmotion=null;

      faceResponses.forEach(response=>{
        gender = response.gender;
        age += response.age;
        Object.keys(response.emotion).map(o=>{
          emotion[o] += response.emotion[o];
        })
      })

      Object.keys(emotion).map(o=>{
        if(emotion[o]/faceResponses.length>confidence){
          confidence=emotion[o]/faceResponses.length;
          majorEmotion=o;
        }
      })

      age = age/faceResponses.length;

      console.log({...info,age,gender,confidence:confidence*100,emotion:majorEmotion});

      setInfo({...info,age,gender,confidence:confidence*100,emotion:majorEmotion});
  }

  const handleAccessToken= async ()=>{
    try {
      let response = await axios.post(`${baseURL}/login`,{
        "userId":"admin_core",
        "password":"2bf3e759dba1b4402707738242f46d16a629f85ab34cf9f964290c9a8c578ef9d6a196d31e694104374d82c302c73bf776f2db831fb7434ca27a3c4ddbb64006"
        },{
            method: 'POST',
          headers:{
            'Content-Type': 'application/json',
          }
      });
      console.log(response,"response------------------->");
      setAccessToken(response.data.loginResponse.data.token);
      return response.data.loginResponse.data.token;
    }catch(err){
      alert("Error in uploading video")
    }
  }

  const handleFaceCapture=()=>{
    console.log("capturinggggggggggggggggggg");
    const canvas = document.createElement("canvas");
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;

    canvas.getContext('2d').drawImage(video,0,0);

    // let dataURL = canvas.toDataURL('image/png');
    canvas.toBlob(async function(blob) {
      var file = new File([blob], `face.png`, {type:"image/png"});
      try{
        var bodyFormData = new FormData();
        bodyFormData.append('file', file);
        let response = await axios({
          method: 'post',
          url:`${baseURL}/API/Feedback/faceAttributes`,
          data:bodyFormData,
          headers:{
            'Content-Type': 'multipart/form-data',
            'token': await handleAccessToken()
          }
        });
        if(response.status==200){
          console.log(response.data,"DFFFFFFFFFFFFFFFFFFFFFFFF");
          setFaceResponses([...faceResponses,...[response.data.faceResponse.faceAttributes]]);
          aggregateFaceResponses([...faceResponses,...[response.data.faceResponse.faceAttributes]]);
          // handleVideoIndex();
          // // setSubmitLoading(false);
          // // setEmoji(true);
        }
      }catch(err){
        alert("Error in uploading face photo")
      }
    }, 'image/png');
    
  }

  const handleVideoIndex = async ()=>{

    // setTimeout(async()=>{
      let response = await axios({
        method: 'post',
        url:`${baseURL}/API/VIDEOINDEXER/videoIndex`,
        data:{
          id:fileName
        },
        headers:{
          'Content-Type': 'application/json',
          'token': await handleAccessToken()
        }
      });

      // if(response.data.state=="Processed"){
        setSubmitLoading(false);
        setRecorded(false);
        setEmoji(true);
        setIndexResponse(response.data);
        console.log(response.data,"Process response--------------------------------->");
      // }
      // else{
      //   console.log("ELSE response",response.data)
      //   handleVideoIndex();
      // }
    // },3000);
  }

  const handleAudioIndex = async ()=>{

    // setTimeout(async()=>{
      let response = await axios({
        method: 'post',
        url:`${baseURL}/API/AUDIOINDEXER/audioIndex`,
        data:{
          id:fileName
        },
        headers:{
          'Content-Type': 'application/json',
          'token': await handleAccessToken()
        }
      });

      // if(response.data.state=="Processed"){
        setSubmitLoading(false);
        // setARecorded(false);
        setEmoji(true);
        setIndexResponse(response.data);
        console.log(response.data,"Process response--------------------------------->");
      // }
      // else{
      //   console.log("ELSE response",response.data)
      //   handleVideoIndex();
      // }
    // },3000);
  }

  const download=()=> {
    theRecorder.stop();
    // theStream.getTracks().forEach(track => {
    //   track.stop();
    // });
  
    var blob = new Blob(recordedChunks, {
      type: 'video/mp4'
    });
    var vfile = new File([blob], `${fileName}.mp4`, {type:"video/mp4"});
    setVideoFile(vfile)
    // console.log(file1,"fileee");
    // sendBlobAsBase64(blob);
   
  }

  // const sendBlobAsBase64=(blob)=> {
  //   const reader = new FileReader();
    
  //   reader.readAsDataURL(blob);
  //   reader.addEventListener('load', () => {
  //     const dataUrl = reader.result;
  //     console.log(dataUrl)
  //     const base64EncodedData = dataUrl.split(',')[1];
  //     // console.log(base64EncodedData,"nbaseeeeeeeee");
      
  //     //sendDataToBackend(base64EncodedData);
  //   });
  
    
  // };

  const handleAudio =(status)=>{
    setAudioError(null)
    if(status=="start"){
      recorder.stop().getMp3().then(([buffer, blob]) => {
        if(document.querySelector('#playlist').hasChildNodes()){
          document.querySelector('#playlist').removeChild(document.querySelector('#playlist').firstChild)
        }
        const file = new File(buffer, `${fileName}.mp3`, {
          type: blob.type,
          lastModified: Date.now()
        });
        setAudioFile(file);
        const li = document.createElement('li');
        const player = new Audio(URL.createObjectURL(file));
        player.controls = true;
        li.appendChild(player);
        document.querySelector('#playlist').appendChild(li);
        setTimer(60);
        setARecorded(true);
        setRecordingStatus(status);
      }).catch((e) => {
        console.error(e);
      });
    
    }
    if(status=="stop"){
      if(document.querySelector('#playlist').hasChildNodes()){
        document.querySelector('#playlist').removeChild(document.querySelector('#playlist').firstChild)
      }
      recorder.start().then(() => {
        setRecordingStatus(status);
        setARecorded(false);
        setFileName(new Date().getTime());
      }).catch((e) => {
        console.error(e);
      });
     
    }
    
  }

  const handleRating=async (userEmotion)=>{
    setFeedbackLoading(true);
    let type;
    if(selected=="video"){
      type="V"
    }
    else if(selected=="audio"){
      type="A"
    }
    else{
      type="T"
    }
    try{
      let response = await axios({
        method: 'post',
        url:`${baseURL}/API/Feedback/submitInteraction`,
        data:{
          ...info,
          type,
          interactionId,
          "customerProvidedSentiment":userEmotion
          // indexId:indexResposne.indexId,
        },
        headers:{
          'Content-Type': 'application/json',
          'token': await handleAccessToken()
        }
      });
      console.log("Submit response-------------------->",response);
      if(response.status==200){
        setFeedbackLoading(false);
        setCompleted(true);
      }
    }catch(err){
      alert("Error in Submitting Feedback")
      setFeedbackLoading(false);
    }
  // else if(selected=="audio"){
  //   try{
  //     let response = await axios({
  //       method: 'post',
  //       url:`${baseURL}/API/AUDIOINDEXER/submit`,
  //       data:{
  //         userEmotion,
  //         indexId:indexResposne.indexId,
  //       },
  //       headers:{
  //         'Content-Type': 'application/json',
  //         'token': await handleAccessToken()
  //       }
  //     });
  //     console.log("Submit response-------------------->",response);
  //     if(response.data.status=="OK"){
  //       setFeedbackLoading(false);
  //       setCompleted(true);
  //     }
  //   }catch(err){
  //     setFeedbackLoading(false);
  //     alert("Error in Submitting Feedback")
  //   }
  // }
  // else{
  //   try{
  //     let response = await axios({
  //       method: 'post',
  //       url:`${baseURL}/API/TEXTINDEXER/submit`,
  //       data:{
  //         userEmotion,
  //         indexId:indexResposne.indexId,
  //       },
  //       headers:{
  //         'Content-Type': 'application/json',
  //         'token': await handleAccessToken()
  //       }
  //     });
  //     console.log("Submit response-------------------->",response);
  //     if(response.data.status=="OK"){
  //       setFeedbackLoading(false);
  //       setCompleted(true);
  //     }
  //   }catch(err){
  //     setFeedbackLoading(false);
  //     alert("Error in Submitting Feedback")
  //   }
  // }
   
  }

  // console.log(faceResponses,"FACE------------------------->")

  return (
    <Container maxWidth="sm">
      <Grid container spacing={2} justify="center">
        <Grid item sm={12} xs={12} alignItems="center" container style={{background:`url(${"/branding-bg.png"}) left repeat-x`,margin:"20px 0px"}}>
        <Grid item sm={6} xs={6}>
          <Box width={1} display="flex" justifyContent="flex-start" mt={2} >
              <img src="/logo2.png" width="90" height="55px" alt=""/>
          </Box>
        </Grid>
        <Grid item sm={6} xs={6}>
          <Box width={1} display="flex" justifyContent="flex-end" mt={2}>
            <img src="/logo1.png" height="" width="140"	height="63px" alt=""/>
          </Box>
        </Grid>
        </Grid>
        <Grid item sm={12} xs={12}>
          <Divider style={{backgroundColor:"#8f754c"}}/>
        </Grid>
        <Grid item sm={12} xs={12} style={{display:"flex",justifyContent:"flex-end"}}>
            <Button variant="text" color="secondary" size="small" aria-controls="simple-menu" aria-haspopup="true" onClick={handleMenuOpen}>
            Language <ArrowDropDownIcon/>
          </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={()=>setAnchorEl(null)}
              
            >
              <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("English")}>English</MenuItem>
              <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("Arabic")}>Arabic</MenuItem>
              <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("Urdu")}>Urdu</MenuItem>
              <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("Hindi")}>Hindi</MenuItem>
            </Menu>
        </Grid>
        <Grid item sm={12} xs={12}>
          <Box width={1} display="flex" justifyContent="center" >
            <Typography variant="h4" style={{fontWeight:"bold"}} color="primary" component="h1"  gutterBottom>
              Suggestions & Feedback
            </Typography>
          </Box>
        </Grid>
        {!completed?<>
        {!emoji?<Grow  in={selected=="text"?true:false} mountOnEnter unmountOnExit>
        <Grid item sm={10} xs={12}>
            <TextField
              id="outlined-multiline-static"
              // label="Type here"
              placeholder="Enter your feedback..."
              fullWidth
              multiline
              rows={10}
              error={textError?true:false}
              value={textValue}
              helperText={textError?textError:`${textValue.length}/250`}
              inputProps={{ maxLength: 250 }}
              onChange={(e)=>{
                setTextError(null);
                setTextValue(e.target.value)
              }}
              // defaultValue="Default Value"
              variant="outlined"
            />
        </Grid>
        </Grow>:null}
        {!emoji?<Grow  in={selected=="video"?true:false} mountOnEnter unmountOnExit>
          <Grid item sm={10} xs={12}>
        {videoPermission?<Grid item sm={12} xs={12} style={{position:"relative"}}>
          <video autoPlay controls={false} muted id="videoElement" width="100%" height="auto" style={{backgroundColor:"#e0e0e0",transform:"scaleX(-1)"}}/>
          {!submitLoading && !emoji?<>
          {recordingStatus=="start"?<Tooltip disableFocusListener disableTouchListener title="Start Recording">
          <Box p="4px" style={{cursor:"pointer"}} onClick={()=>handleRecording("stop")} bottom="7%" right="47%" width="35px" border="1" bgcolor="white" borderRadius="50%" display="flex" justifyContent="center" position="absolute">
            <Box bgcolor="red" width="30px" height="26px" borderRadius="50%" />
          </Box>
          </Tooltip>:<Tooltip disableFocusListener disableTouchListener title="Stop Recording">
          <Box p="8px" style={{cursor:"pointer"}} onClick={()=>handleRecording("start")} bottom="7%" right="47%" width="35px" border="1" bgcolor="white" borderRadius="50%" display="flex" justifyContent="center" position="absolute">
            <Box bgcolor="red" width="20px" height="18px"  />
          </Box>
          </Tooltip>}
          </>:null}
          {recordingStatus=="stop"?<Box position="absolute" display="flex" top="3%" right="5%" alignItems="center">
            <Box bgcolor="#ff0000a1" width="12px" height="12px" borderRadius="50%" /> <span style={{color:"white",fontWeight:"bold",fontSize:"18px",paddingLeft:"5px"}}>{timer}s</span>
          </Box>:null}
        </Grid>:<Grid>
        <Typography variant="h6" color="textSecondary" >
            Please allow camera to capture your video
          </Typography>
          </Grid>}
        {recorded?<Grid item sm={12} xs={12}>
         <Typography variant="caption" color="textSecondary" >
            Your video is recorded successfully
          </Typography>
        </Grid>:null}
        {videoError?<Grid item sm={12} xs={12}>
         <Typography variant="caption" style={{color:"#f44336"}} >
            {videoError}
          </Typography>
        </Grid>:null}
        </Grid>
        </Grow>:null}
        {!emoji?<Grow  in={selected=="audio"?true:false} mountOnEnter unmountOnExit>
        <Grid  item container sm={10} xs={12} >
          <Box border={1} borderColor="#d5d5d5" width={1} borderRadius="4px" position="relative">
            {!emoji?<Box>
            {recordingStatus=="start"?<Grid item sm={12} xs={12} style={{display:"flex",justifyContent:"center",margin:"20px 0px"}}>
            <Tooltip disableFocusListener disableTouchListener title="Start Recording">
                <img src="/mic-on.png" style={{cursor:"pointer",height:"125px",width:"125px"}} onClick={()=>handleAudio("stop")}/>
                </Tooltip>
            </Grid>:
            <Grid item sm={12} xs={12} style={{display:"flex",justifyContent:"center",margin:"20px 0px"}}>
               <Tooltip disableFocusListener disableTouchListener title="Stop Recording">
                <img src="/stop.png" style={{cursor:"pointer",height:"125px",width:"125px"}} onClick={()=>handleAudio("start")}/>
                </Tooltip>
              
            </Grid>}
            </Box>:null}
            {recordingStatus=="stop"?<Box position="absolute" display="flex" top="3%" right="5%" alignItems="center">
            <Box bgcolor="#ff0000a1" width="12px" height="12px" borderRadius="50%" /> <span style={{color:"rgba(108, 117, 125, 0.5)",fontWeight:"bold",fontSize:"18px",paddingLeft:"5px"}}>{timer}s</span>
          </Box>:null}
            <Grid item sm={12} xs={12} style={{display:arecorded?"flex":"none",justifyContent:"center"}}>
            <ul id="playlist" style={{padding:0,listStyleType:"none"}}></ul>
            </Grid>
            </Box>
        {audioError?<Grid item sm={12} xs={12}>
         <Typography variant="caption" style={{color:"#f44336"}} >
            {audioError}
          </Typography>
        </Grid>:null}
        </Grid>
        </Grow>:null}
        {!emoji && !submitLoading?<Grid item container sm={10} xs={12} alignItems="center" >
          <ButtonGroup  fullWidth aria-label="outlined primary button group">
            <Button style={{backgroundColor:selected=="text"?"#8f754cba":"#8f754c",color:"white"}} variant="contained" disabled={emoji||submitLoading} onClick={()=>handleOption("text")} ><ChatBubbleIcon style={{marginRight:"4px"}}/>Text</Button>
            <Button style={{backgroundColor:selected=="video"?"#337ab7ab":"#337ab7",color:"white"}} variant={selected=="video"?"contained":"outlined"} disabled={emoji || submitLoading}  onClick={()=>handleOption("video")}><VideocamIcon style={{marginRight:"4px"}}/>Video</Button>
            <Button style={{backgroundColor:selected=="audio"?"#454545a6":"#454545",color:"white"}} variant={selected=="audio"?"contained":"outlined"} disabled={emoji || submitLoading} onClick={()=>handleOption("audio")}><MicIcon style={{marginRight:"4px"}}/>Audio</Button>
          </ButtonGroup>
        </Grid>:null}
        {!emoji?<>
        <Grid item sm={10} xs={12}>
          <Button variant="contained" color="secondary" disabled={submitLoading} size="large" fullWidth onClick={handleSubmit}>
            {submitLoading?
            <>Submitting Feedback<CircularProgress style={{marginLeft:10}} size={20} /></>:<>Submit<TelegramIcon/></>}
          </Button>
        </Grid></>:
        !feedbackLoading?<Grid item container sm={10} spacing={1} xs={12} alignItems="center" justify="center" style={{marginBottom:"40px"}}>
                  <Grid item sm={12} xs={12}>
                  <FormControl variant="outlined" style={{width:"100%"}}>
                    <InputLabel htmlFor="outlined-nat-native-simple">Nationality</InputLabel>
                    <Select
                      native
                      value={info.nationality}
                      onChange={(e)=>{
                        setInfo({...info,nationality:e.target.value})
                      }}
                      label="Nationality"
                      inputProps={{
                        name: 'nationality',
                        id: 'outlined-nat-native-simple',
                      }}
                    >
                      <option aria-label="None" value="" />
                      <option value={"bahrain"}>Bahrain</option>
                      <option value={"pakistan"}>Pakistan</option>
                      <option value={"oman"}>Oman</option>
                      <option value={"uae"}>United Arab Emirates</option>
                      <option value={"bahrain"}>Bahrain</option>
                      <option value={"kuwait"}>Kuwait</option>
                      <option value={"qatar"}>Qatar</option>
                      <option value={"ksa"}>Saudi Arabia</option>
                    </Select>
                  </FormControl>
          </Grid>
          {selected!="video"?<>
          <Grid item sm={6} xs={6}>
              <TextField
                  label="Age"
           
                  fullWidth
                  type="number"
                  // size="small"
                  // style={{marginBottom:"6px"}}
                  // error={textError?true:false}
                  value={info.age}
                  // helperText={textError?textError:`${textValue.length}/250`}
                  onChange={(e)=>{
                    setInfo({...info,age:e.target.value})
                  }}
                  // defaultValue="Default Value"
                  variant="outlined"
                />
          </Grid>
          <Grid item sm={6} xs={6}>
          <FormControl variant="outlined" style={{width:"100%"}}>
                    <InputLabel htmlFor="outlined-nat-native-simple">Gender</InputLabel>
                    <Select
                      native
                      value={info.gender}
                      onChange={(e)=>{
                        setInfo({...info,gender:e.target.value})
                      }}
                      label="Gender"
                      inputProps={{
                        name: 'gender',
                        id: 'outlined-gender-native-simple',
                      }}
                    >
                      <option aria-label="None" value="" />
                      <option value={"male"}>Male</option>
                      <option value={"female"}>Female</option>
                      <option value={"other"}>Other</option>
                    </Select>
                  </FormControl>
             
          </Grid>
          </>:null}
                  <Grid item sm={12} xs={12} style={{marginBottom:"20px",textAlign:"center"}}>
                  {/* {indexResposne?<Typography variant="body2" gutterBottom style={{color:"#454552"}}>
                      {indexResposne.majorSentiment.toUpperCase() =="NEUTRAL" && `We have observed from your feedback that you are feeling Neutral`}
                      {indexResposne.majorSentiment.toUpperCase()=="POSITIVE" && `We have observed from your feedback that you are feeling Happy`}
                      {indexResposne.majorSentiment.toUpperCase()=="NEGATIVE" && `We have observed from your feedback that you are feeling Sad`}
                    </Typography>:null} */}
                    <Typography variant="body2" gutterBottom style={{color:"#454552"}}>
                      Please select your satisfaction level to continue
                    </Typography>
                  </Grid>
                  <Grid item  style={{display:"flex",justifyContent:"center"}}>
                  {/* <img src="/happy.png" width="54" height="54"/> */}
                  <div className="example-block" onClick={()=>handleRating('Happy')}><label className="radio-inline"> <input type="radio" name="emotion" id="sad" className="input-hidden" /><img src="/happy.png" width="70" height="70"/></label></div>
                  </Grid>
                  <Grid item  style={{display:"flex",justifyContent:"center"}}>
                  <div className="example-block" onClick={()=>handleRating('Neutral')}><label className="radio-inline"> <input type="radio" name="emotion" id="sad" className="input-hidden" /><img src="/neutral.png" width="70" height="70"/></label></div>
                  </Grid>
                  <Grid item  style={{display:"flex",justifyContent:"center"}}>
                  <div className="example-block" onClick={()=>handleRating('Sad')}><label className="radio-inline"> <input type="radio" name="emotion" id="sad" className="input-hidden" /><img src="/sad.png" width="70" height="70"/></label></div>
                  </Grid>
            </Grid>:<Grid item container sm={10} xs={12} alignItems="center" justify="center" style={{marginBottom:"40px"}}>
              <CircularProgress szie={30}/>
            </Grid>
          } 
            </>:
            <Grid item sm={10} xs={12} style={{display:"flex",justifyContent:"center",marginTop:"20px"}}>
              <Typography variant="h6" color="primary">
                Your feedback is submitted. Thank you.
              </Typography>
              </Grid>}       
            </Grid>
      
    </Container>
  );
}
