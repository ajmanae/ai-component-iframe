import React,{useState,useEffect} from 'react';
import { withStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router'
import {IconButton,Typography,Tooltip,Menu,MenuItem,Checkbox,CircularProgress,Box,TextField,Button,Grid,Dialog,DialogTitle as MuiDialogTitle,DialogContent as MuiDialogContent,DialogActions as MuiDialogActions,Paper} from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import axios from "axios";
import VideocamIcon from '@material-ui/icons/Videocam';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';


const baseURL = "https://customer-happiness.uaenorth.cloudapp.azure.com/api"

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.root} {...other}>
        <Typography variant="h6" style={{color:props.color||"#5ED4E4"}}>{children}</Typography>
        {/* {onClose ? (
          <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
            <CancelIcon style={{color:"#1762B8"}} />
          </IconButton>
        ) : null} */}
      </MuiDialogTitle>
    );
  });
const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

var theStream;
let video;
var theRecorder;
var recordedChunks = [];
var allEmotions=[];

export default function VideoModal() {


    const [recorded,setRecorded]=useState(false);
    const [open,setOpen]=useState(true)
    const [videoPermission,setVideoPermission]=useState(true);
    const [videoError,setVideoError]=useState(null);
    const [loaded,setLoaded]=useState(false);
    const [videoFile,setVideoFile]=useState(null);
    const [info,setInfo]=useState({
      nationality:"",
      gender:null,
      emotion:null,
      confidence:0,
      age:"",
    });
    const [faceResponses,setFaceResponses]=useState([]);
  const [timer,setTimer]=useState(60);
  const [fileName,setFileName]=useState(null);
  const [recordingStatus,setRecordingStatus]=useState("start");
  const [submitLoading,setSubmitLoading]=useState(false);
  const [interactionId,setInteractionId] = useState(null);
  const [satisfaction,setSatisfaction] = useState(null);
  const [feedbackLoading,setFeedbackLoading]=useState(false);
  const [completed,setCompleted]=useState(false);
  const [other,setOther]=useState(false);
  const [accessToken,setAccessToken]=useState("");
  const [captured,setCaptured]=useState(null);
  const [anchorEl, setAnchorEl] = useState(null);
  const [language,setLanguage] = useState("en-US");
  const [lname,setlname]=useState("English");
  const [disc,setDisc] = useState(false);
  // const [allEmotions,setAllEmotions]=useState([]);
  const [userConfig,setUserConfig]=useState({
    clientKey:"3d44a382820ca884b74110fc1eebe5984f524343f78e58bd5958ea56fbf195907203c6b4b99fc40d6c5d18ec12bf41f9bde3aa84a47bf6a7b1bbb107c07937a8",
    clientSecret:"70747a4ad98badf11465ae80f45e047a1832f1805ebe91b473295d92500a922dd9b6b41c91ebd34beb0f692b40a303a27a6910f5075c6b6897da639e85335463",
    heading:"Suggestions & Feedback",
    headingColor:"#5ED4E4",
    primaryColor:"#1762B8",
    language:"en-US",
    gender:"male"
});
const [configError,setConfigError]=useState(false)
const {query} = useRouter();
  let interval;

 
  useEffect(()=>{
    if(query["clientKey"] && query["clientSecret"]){
        // console.log(query);
        setConfigError(false);
        setUserConfig({
            ...userConfig,
            ...query
        })
    }
    if(!query["clientKey"] || !query["clientSecret"]){
        setConfigError(true);
    }
    if(open && !loaded){
        video = document.querySelector("#videoElement");
        if (navigator.mediaDevices.getUserMedia) {
          navigator.mediaDevices.getUserMedia({ video: true,audio:true })
            .then((stream)=> {
              theStream = stream;
              video.srcObject = stream;
              setVideoPermission(true);
              try {
                var recorder = new MediaRecorder(stream, {
                  mimeType: "video/webm"
                });
              } catch (e) {
                console.error('Exception while creating MediaRecorder: ' + e);
                return;
              }
            
              theRecorder = recorder;
              recorder.ondataavailable =
                (event) => {
                  recordedChunks.push(event.data);
                };
              // recorder.start(100);
              setLoaded(true);
             
            })
            .catch(function (error) {
              setVideoPermission(false)
              console.log("Something went wrong!",error);
            });
        }
       
    }
    if(recordingStatus=="stop"){
       interval = setInterval(() => {
        setTimer(timer-1);
      }, 1000);
    }
    if(timer==0){
      handleRecording("start");
    }
    if((timer%5==0) && (timer!=60)){
      handleFaceCapture();
    }

    return () => clearInterval(interval);
   

  },[timer,recordingStatus,loaded,videoPermission,query])


  const handleRecording =(status)=>{
    setVideoError(null);
    if(status=="start"){
      // if(!faceResponses.length){
      //   alert("Sorry, could detect face. Please record the video again.");
      // }
      setTimer(60);
      setRecorded(true);
      download();
      //aggregateFaceResponses();
    }
    if(status=="stop"){
      setRecorded(false);
      setFileName(new Date().getTime());
      theRecorder.start(100)
    }
    setRecordingStatus(status);
  }

  // const handleAccessToken= async ()=>{
  //   try {
  //     let response = await axios.post(`${baseURL}/login`,{
  //       "userId":"admin_core",
  //       "password":"2bf3e759dba1b4402707738242f46d16a629f85ab34cf9f964290c9a8c578ef9d6a196d31e694104374d82c302c73bf776f2db831fb7434ca27a3c4ddbb64006"
  //       },{
  //           method: 'POST',
  //         headers:{
  //           'Content-Type': 'application/json',
  //         }
  //     });
  //     console.log(response,"response------------------->");
  //     setAccessToken(response.data.loginResponse.data.token);
  //     return response.data.loginResponse.data.token;
  //   }catch(err){
  //       setSubmitLoading(false);
  //       setTextError("Error in uploading Feedback");
  //   }
  // }

  const handleSubmit =async ()=>{
    setVideoError(null);
    // let cinteractionId=`V-${new Date().getTime()}`;
    if(!recorded){
        setVideoError("Please record your video first!")
    }
    else{
        setSubmitLoading(true);
        // let token = await handleAccessToken();
        try{
          var bodyFormData = new FormData();
          bodyFormData.append('file', captured);
          bodyFormData.append('type', "V");
          let response = await axios({
            method: 'post',
            url:`${baseURL}/PUBLIC/Feedback/quickFeedback`,
            data:bodyFormData,
            headers:{
              'Content-Type': 'multipart/form-data',
              'clientKey':userConfig.clientKey,
              'clientSecret':userConfig.clientSecret
            }
          });
          if(response.status==200){
            console.log(response.data,"DFFFFFFFFFFFFFFFFFFFFFFFF");
            if(!allEmotions.length){
              allEmotions=[response.data.emotion];
              setInfo({
                ...info,
                age:response.data.age,
                gender:response.data.gender,
                emotion:response.data.emotion
              })
            }
          
            try{
              var bodyFormData1 = new FormData();
              bodyFormData1.append('name', fileName);
              bodyFormData1.append('file', videoFile);
              bodyFormData1.append('type', "V");
              bodyFormData1.append('orgId', "AFZ");
              bodyFormData1.append('language', language);
              bodyFormData1.append('interactionId', response.data.interactionId);
              if(allEmotions.includes("negative")){
                setInfo({...info,emotion:"negative"})
              }
              if(allEmotions.includes("positive") && allEmotions.includes("neutral") && !allEmotions.includes("negative")){
                setInfo({...info,emotion:"positive"})
              }
              // setSubmitLoading(false);
              //   setRecorded(false);
              //   setInteractionId(cinteractionId);
              let response1 = await axios({
                method: 'post',
                url:`${baseURL}/PUBLIC/Feedback/uploadInteraction`,
                data:bodyFormData1,
                headers:{
                  'Content-Type': 'multipart/form-data',
                  'clientKey':userConfig.clientKey,
                  'clientSecret':userConfig.clientSecret
                }
              });
              console.log("upload response",response1);
              setInteractionId(response.data.interactionId);
              setSubmitLoading(false);
              setRecorded(false);
            }catch(err){
              console.log("Error in uploading Video",err);
            }
          }
        }catch(err){
          console.log("Error in uploading Video",err);
          setSubmitLoading(false);
          setVideoError("Error in uploading video");
        }
    }
   
  }

  const aggregateFaceResponses=(faceResponses)=>{
    let emotion={
      // anger: 0,
      // contempt: 0,
      // disgust: 0,
      // fear: 0,
      // sadness: 0,
      // happiness: 0,
      // surprise: 0,
      negative:0,
      positive:0,
      neutral: 0,
    };
    let confidence=0;
    let age=0;
    let gender=null;
    let majorEmotion=null;

    faceResponses.forEach(response=>{
      //console.log(response.emotion);
      gender = response.gender;
      age += response.age;
    });

    let response = faceResponses[faceResponses.length-1];

    Object.keys(response.emotion).map(o=>{
      if(o=="anger" || o=="contempt" || o=="disgust" || o=="fear" || o=="sadness"){
        //console.log("negtiave case---->",o)
        emotion["negative"] += response.emotion[o];
      }
      else if(o=="happiness" || o=="surprise"){
        //console.log("postive case---->",o)
        emotion["positive"] += response.emotion[o];
      }
      else{
        //console.log("neutral case---->",o)
        emotion["neutral"] += response.emotion[o];
      }
      
    })

    console.log("all scores---------->",emotion)
    console.log("total confidence",)

    Object.keys(emotion).map(o=>{
    //   console.log(o,"emotion key");
    //   console.log(confidence,"confidence");
    //   console.log(majorEmotion,"majorEmotion");
      if((emotion[o]>confidence) && (o !="neutral")){
        confidence=emotion[o];
        majorEmotion=o;
      }
    })

    age = age/faceResponses.length;

    console.log({...info,age,gender,confidence:confidence*100,emotion:majorEmotion});

    allEmotions=[...allEmotions,...[majorEmotion]];

    console.log("all Emotions----------->",allEmotions);
    // setAllEmotions(allEmotions1);

    setInfo({...info,age,gender,confidence:confidence*100,emotion:majorEmotion});
}

const handleFaceCapture=()=>{
    console.log("capturinggggggggggggggggggg");
    const canvas = document.createElement("canvas");
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;

    canvas.getContext('2d').drawImage(video,0,0);

    // let dataURL = canvas.toDataURL('image/png');
    canvas.toBlob(async function(blob) {
      var file = new File([blob], `face.png`, {type:"image/png"});
      setCaptured(file);
      try{
        var bodyFormData = new FormData();
        bodyFormData.append('file', file);
        let response = await axios({
          method: 'post',
          url:`${baseURL}/PUBLIC/Feedback/faceAttributes`,
          data:bodyFormData,
          headers:{
            'Content-Type': 'multipart/form-data',
            'clientKey':userConfig.clientKey,
            'clientSecret':userConfig.clientSecret
          }
        });
        if(response.status==200){
          console.log(response.data,"DFFFFFFFFFFFFFFFFFFFFFFFF");
          setFaceResponses([...faceResponses,...[response.data.faceResponse.faceAttributes]]);
          aggregateFaceResponses([...faceResponses,...[response.data.faceResponse.faceAttributes]]);
          // handleVideoIndex();
          // // setSubmitLoading(false);
          // // setinteractionId(true);
        }
      }catch(err){
        console.log("Error in uploading face photo",err)
      }
    }, 'image/png');
    
  }

  
  const download=()=> {
    theRecorder.stop();
    // theStream.getTracks().forEach(track => {
    //   track.stop();
    // });
  
    var blob = new Blob(recordedChunks, {
      type: 'video/mp4'
    });
    var vfile = new File([blob], `${fileName}.mp4`, {type:"video/mp4"});
    setVideoFile(vfile)
    // console.log(file1,"fileee");
    // sendBlobAsBase64(blob);
   
  }

  const handleSatisfaction = async (value,emotion)=>{
      console.log("Satisfaction called",value,emotion);
    if(value!="No"){
        setFeedbackLoading(true);
    }
    // let userEmotion;
    // if(emotion=="neutral"){
    //     userEmotion="neutral"
    // }
    // if(emotion=="happiness"){
    //     userEmotion="positive"
    // }
    // if(emotion=="sadness"){
    //     userEmotion="negative"
    // }
    let type = "V";
    
    if(value=="No"){
        setOther(true);
    }
    if(value=="Yes"){
        try{
          console.log(interactionId,"intereactionId---------------------")
            let response = await axios({
              method: 'post',
              url:`${baseURL}/PUBLIC/Feedback/submitInteraction`,
              data:{
                ...info,
                type,
                interactionId,
                "customerProvidedSentiment":emotion
                // indexId:indexResposne.indexId,
              },
              headers:{
                'Content-Type': 'application/json',
                'clientKey':userConfig.clientKey,
                'clientSecret':userConfig.clientSecret
              }
            });
            console.log("Submit response-------------------->",response);
            if(response.status==200){
              setFeedbackLoading(false);
              setCompleted(true);
            }
          }catch(err){
            alert("Error in Submitting Feedback")
            setFeedbackLoading(false);
          }
    }
    
  }

  
  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };
  
  const handleLanguage= (language,name) => {
    setLanguage(language);
    setlname(name);
    // if(language!="English"){
    //   document.getElementsByTagName('html')[0].setAttribute("dir", "rtl");
    // }
    // else{
    //   document.getElementsByTagName('html')[0].setAttribute("dir", "ltr");
    // }
    setAnchorEl(null);
  };

  const handleClose = () =>{
    setOpen(false);
}

  
 
  return (
    <div style={{display:"flex",alignItems:"center",justifyContent:"center",margin:"auto"}}>
    {!configError?<div style={{width:"100%",position:"relative"}}>
        <Paper>
        <DialogTitle id="customized-dialog-title2" color={userConfig.headingColor} onClose={handleClose}>
        {userConfig.heading}
        </DialogTitle>
        <DialogContent dividers style={{borderBottom:"none"}}>
        {!interactionId?<>
            <Grid item  xs={12}>
            <Grid item  xs={12} style={{display:"flex",justifyContent:"center",margin:"5px 0px 0px 0px",textAlign:"center"}}>
                  <Typography variant="body2" color="textSecondary" >
                    Press the below button to record your feedback in
                  </Typography>
                </Grid>
                <Grid item  xs={12} style={{display:"flex",justifyContent:"center",margin:"0px 0px 0px 0px",textAlign:"center"}}>
                <Button variant="text" style={{color:userConfig.primaryColor}} size="small" aria-controls="simple-menu" aria-haspopup="true" onClick={handleMenuOpen}>
                  {lname} <ArrowDropDownIcon/>
                </Button>
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={()=>setAnchorEl(null)}
                    
                  >
                    <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("en-US","English")}>English</MenuItem>
                    <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("ar-EG","Arabic")}>Arabic</MenuItem>
                    <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("hi-IN","Urdu")}>Urdu</MenuItem>
                    <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("hi-IN","Hindi")}>Hindi</MenuItem>
                    <MenuItem dense style={{color:"grey"}} onClick={()=>handleLanguage("auto","Other")}>Other</MenuItem>
                  </Menu>
                </Grid>
       <Grid item  xs={12} style={{position:"relative"}}>
          <video autoPlay controls={false} muted id="videoElement" width="100%" height="auto" style={{backgroundColor:"#e0e0e0",transform:"scaleX(-1)",display:"block"}}/>
          {!submitLoading && !interactionId && videoPermission?<>
          {recordingStatus=="start"?<Tooltip disableFocusListener disableTouchListener title="Start Recording">
          <Box p="4px" style={{cursor:"pointer"}} onClick={()=>handleRecording("stop")} bottom="7%" right="47%" width="35px" border="1" bgcolor="white" borderRadius="50%" display="flex" justifyContent="center" position="absolute">
            <Box bgcolor="red" width="30px" height="26px" borderRadius="50%" />
          </Box>
          </Tooltip>:<Tooltip disableFocusListener disableTouchListener title="Stop Recording">
          <Box p="8px" style={{cursor:"pointer"}} onClick={()=>handleRecording("start")} bottom="7%" right="47%" width="35px" border="1" bgcolor="white" borderRadius="50%" display="flex" justifyContent="center" position="absolute">
            <Box bgcolor="red" width="20px" height="18px"  />
          </Box>
          </Tooltip>}
          </>:null}
          {recordingStatus=="stop"?<Box position="absolute" display="flex" top="3%" right="5%" alignItems="center">
            <Box bgcolor="#ff0000a1" width="12px" height="12px" borderRadius="50%" /> <span style={{color:"white",fontWeight:"bold",fontSize:"18px",paddingLeft:"5px"}}>{timer}s</span>
          </Box>:null}
        </Grid>
        <Box width="100%" bgcolor={userConfig.primaryColor} color="white" py={0.5} display="flex" justifyContent="center" alignItems="center">
                <VideocamIcon fontSize="small" style={{margin:"0px 5px"}}/> Video
            </Box>
        {recorded?<Grid item sm={12} xs={12}>
         <Typography variant="caption" color="textSecondary" >
            Your video is recorded successfully
          </Typography>
        </Grid>:null}
        {videoError?<Grid item sm={12} xs={12}>
         <Typography variant="caption" style={{color:"#f44336"}} >
            {videoError}
          </Typography>
        </Grid>:null}
        </Grid>
        <Grid item xs={12} style={{display:"flex",alignItems:"center",paddingTop:"10px"}}>
        <Checkbox style={{color:userConfig.primaryColor}} checked={disc} onChange={(e)=>setDisc(e.target.checked)} name="disc" />
        <Typography variant="caption" color="textSecondary">
            I consent usage of this recorded data for the purpose of quality assurance
            </Typography>
          
        </Grid>
        <Grid item xs={12} style={{display:"flex",justifyContent:"center",alignItems:"center"}}>
            <Box width="50%" pt={2} >
                <Button size="small" onClick={handleSubmit} disabled={!disc || submitLoading} variant="contained" style={!disc || submitLoading?{}:{backgroundColor:userConfig.primaryColor,color:"white"}} fullWidth   >Submit{submitLoading?<CircularProgress style={{marginLeft:10,color:"white"}} size={20}  />:null}</Button>
            </Box>
        </Grid></>:
        <>
        {/* !other || !completed  */}
            {!completed ?!other?<><Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                {info.emotion=="positive" || info.emotion=="neutral"?"Satisfied with our services?":"Unsatisfied with our services?"}
                </Typography>
            </Grid>
            <Grid item xs={12} style={{textAlign:"center"}}>
              {info.gender=="male"  || info.gender==null?
              <>
                {info.emotion=="positive" || info.emotion=="neutral"?
                 <Box style={{textAlign:"center"}}>
                 <Box>
                 <img src="man-smile1.png" width="80" height="80" />
               </Box>
               {/* <Typography variant="caption" color="textSecondary" gutterBottom>
                 Satisfied
               </Typography> */}
             </Box>:null}
               {/* {info.emotion=="neutral"?
                <Box  style={{textAlign:"center"}}>
                <Box>
                <img src="man-smile2.png" width="80" height="80" />
              </Box>
            </Box> 
               :null} */}
               {info.emotion=="negative"?
               <Box style={{textAlign:"center"}}>
               <Box>
               <img src="man-smile3.png" width="80" height="80" />
             </Box>
             {/* <Typography variant="caption" color="textSecondary" gutterBottom>
               Unsatisfied
             </Typography> */}
           </Box>
               :null}
               {!["positive","neutral","negative"].includes(info.emotion)?<Box  style={{textAlign:"center"}}>
                <Box>
                <img src="man-smile2.png" width="80" height="80" />
              </Box>
              {/* <Typography variant="caption" color="textSecondary" gutterBottom>
                I'm OK
              </Typography> */}
            </Box> :null}
               </>: <>
               {info.emotion=="positive" || info.emotion=="neutral"?
                 <Box style={{textAlign:"center"}}>
                 <Box>
                 <img src="woman-smile1.png" width="80" height="80" />
               </Box>
               {/* <Typography variant="caption" color="textSecondary" gutterBottom>
                 Satisfied
               </Typography> */}
             </Box>:null}
               {/* {info.emotion=="neutral"?
                <Box  style={{textAlign:"center"}}>
                <Box>
                <img src="woman-smile2.png" width="80" height="80" />
              </Box>
            </Box> 
               :null} */}
               {info.emotion=="negative"?
               <Box style={{textAlign:"center"}}>
               <Box>
               <img src="woman-smile3.png" width="80" height="80" />
             </Box>
             {/* <Typography variant="caption" color="textSecondary" gutterBottom>
               Unsatisfied
             </Typography> */}
           </Box>
               :null}
               {!["positive","neutral","negative"].includes(info.emotion)?<Box  style={{textAlign:"center"}}>
                <Box>
                <img src="woman-smile2.png" width="80" height="80" />
              </Box>
              {/* <Typography variant="caption" color="textSecondary" gutterBottom>
                I'm OK
              </Typography> */}
            </Box> :null}
               
               </>}
            </Grid>
            <Grid item xs={12} style={{display:"flex",justifyContent:"center",alignItems:"center",padding:"20px 0px"}}>
               <Button disabled={feedbackLoading} onClick={()=>handleSatisfaction("Yes",(info.emotion=="positive" || info.emotion=="neutral")?"positive":info.emotion)} size="small" variant="contained" style={{backgroundColor:userConfig.primaryColor,color:"white",marginRight:"5px"}}>
                   Yes{feedbackLoading?<CircularProgress style={{marginLeft:10,color:"white"}}size={15}  />:null}
               </Button>
               <Button disabled={feedbackLoading} onClick={()=>handleSatisfaction("No")} size="small" variant="contained" style={{backgroundColor:userConfig.primaryColor,color:"white",marginLeft:"5px"}} >
                   No
               </Button>
            </Grid></>:
             !feedbackLoading?<><Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                    Provide your feedback
                </Typography>
            </Grid>
            <Grid item xs={12} style={{textAlign:"center",display:"flex",justifyContent:"center"}}>
            {info.gender=="male" || info.gender==null?<>
            {["positive","neutral","negative"].includes(info.emotion)?<>
            {/* {info.emotion!="positive" &&
             <Box style={{textAlign:"center"}}>
             <Box>
             <img src="man-smile1.png" onClick={()=>handleSatisfaction("Yes","positive")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
           </Box>
           <Typography variant="caption" color="textSecondary" gutterBottom>
             Satisfied
           </Typography>
         </Box> 
             
            } */}
            {(info.emotion!="neutral" || info.emotion!="positive") &&
            <Box style={{textAlign:"center"}}>
            <Box>
            <img src="man-smile2.png" onClick={()=>handleSatisfaction("Yes","neutral")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
          </Box>
          <Typography variant="caption" color="textSecondary" gutterBottom>
            I'm OK
          </Typography>
        </Box> 
            }
            {info.emotion!="negative"  && 
            <Box style={{textAlign:"center"}}>
            <Box>
            <img src="man-smile3.png" onClick={()=>handleSatisfaction("Yes","negative")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
          </Box>
          <Typography variant="caption" color="textSecondary" gutterBottom>
            Unsatisfied
          </Typography>
        </Box> 
            }
            </>:<>
            {
               <Box style={{textAlign:"center"}}>
               <Box>
               <img src="man-smile1.png" onClick={()=>handleSatisfaction("Yes","positive")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
             </Box>
             <Typography variant="caption" color="textSecondary" gutterBottom>
               Satisfied
             </Typography>
           </Box> 
            }
            {<Box style={{textAlign:"center"}}>
            <Box>
            <img src="man-smile3.png" onClick={()=>handleSatisfaction("Yes","negative")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
          </Box>
          <Typography variant="caption" color="textSecondary" gutterBottom>
            Unsatisfied
          </Typography>
        </Box>}
            </>}
            </>:["positive","neutral","negative"].includes(info.emotion)?<>
            {/* {info.emotion!="positive" && <Box style={{textAlign:"center"}}>
               <Box>
               <img src="woman-smile1.png" onClick={()=>handleSatisfaction("Yes","positive")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
             </Box>
             <Typography variant="caption" color="textSecondary" gutterBottom>
               Satisfied
             </Typography>
           </Box> } */}
            {(info.emotion!="neutral" || info.emotion!="positive") && <Box style={{textAlign:"center"}}>
             <Box>
             <img src="woman-smile2.png" onClick={()=>handleSatisfaction("Yes","neutral")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
           </Box>
           <Typography variant="caption" color="textSecondary" gutterBottom>
           I'm OK
           </Typography>
         </Box> }
            {info.emotion!="negative"  && 
             <Box style={{textAlign:"center"}}>
             <Box>
             <img src="woman-smile3.png" onClick={()=>handleSatisfaction("Yes","negative")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
           </Box>
           <Typography variant="caption" color="textSecondary" gutterBottom>
           Unsatisfied
           </Typography>
         </Box> }
            </>:<>
            {<Box style={{textAlign:"center"}}>
               <Box>
               <img src="woman-smile1.png" onClick={()=>handleSatisfaction("Yes","positive")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
             </Box>
             <Typography variant="caption" color="textSecondary" gutterBottom>
               Satisfied
             </Typography>
           </Box> }
            {<Box style={{textAlign:"center"}}>
             <Box>
             <img src="woman-smile3.png" onClick={()=>handleSatisfaction("Yes","negative")} width="80" height="80" style={{marginRight:"5px",cursor:"pointer"}}/>
           </Box>
           <Typography variant="caption" color="textSecondary" gutterBottom>
           Unsatisfied
           </Typography>
         </Box>}
            </>}
            </Grid>
            </>:<><Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                    Submitting your feedback
                </Typography>
            </Grid>
            <Grid item xs={12} style={{textAlign:"center"}}>
                <CircularProgress style={{marginLeft:10}} color="primary" size={30}  />
            </Grid>
            </>
            :<Grid item xs={12} style={{textAlign:"center"}}>
                <Typography color="textSecondary" gutterBottom variant="body1">
                    Your feedback is submitted successfully
                </Typography>
            </Grid>}
            
        </>}
        </DialogContent>
        </Paper>
      </div>:<div  style={{width:"100%",padding:"20px 0px",textAlign:"center"}}>
          Provided Configuration is not valid
    </div>}
    </div>
  );
}